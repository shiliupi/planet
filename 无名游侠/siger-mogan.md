《[莫干山 沁入竹林](https://fotomen.cn/2012/03/30/hangzhou-2/)》[孟昕雨](https://fotomen.cn/author/mengxinyu/) 摄

@[沈浪熊猫儿](https://gitee.com/darcyshen) ：

> 墨干得名于家乡的避暑胜地——莫干山。犹忆青春年少时，在高中同桌老家，靠近莫干山的一个小山村里面，住过一晚上。傍晚，我们就在屋后小溪里面洗澡。清晨，我们跟着他在山上兜兜转转挖笋。中午，一起吃刚挖好的笋。溪水潺潺，篁竹幽幽，炊烟袅袅……

让所有人畅快地学习既有的科学与技术，创造全新的科学与技术。

- [墨干编辑器](https://gitee.com/XmacsLabs/mogan) [里程碑版本 V1.0.4 发布](https://gitee.com/shiliupi/mogan/issues/I5CY2N)
- [墨客星球](https://gitee.com/XmacsLabs/planet) 上线：聚合 TeXmacs 文档
- SIGer 专访 [墨者实验室](https://gitee.com/XmacsLabs)主理人：《[代码——纯文本已经满足不了“我”的野心](https://www.zhihu.com/question/55655088/answer/763551807)》

<p><img width="706px" src="https://images.gitee.com/uploads/images/2022/0624/090909_e530fead_5631341.jpeg" title="mogan.jpg"></p>

> 同为行星物种对各色星球天生的好感，今天扮演一下星球君，为同学们呈现的是 “墨客星球”。精确的定义留给受访人，我只说说是怎么的机缘与君相识的 :pray:

Gitee 星球同学们已经不陌生，日常打开自己的工作台，我都会看下右下脚的 最新到访，之后是推荐项目，推荐开发者。物以类聚，同是开发者的你我，各自不同兴趣研究方向，要能相聚形成交集也不容易，SIGer 通过到访和推荐确实遇到了几个不错的项目和主理人。“墨客星球” 的主理 @[沈浪熊猫儿](https://gitee.com/darcyshen) 就算一位。

> 说来也巧，SIGer 完成[全年立项主题任务](https://gitee.com/flame-ai/siger/blob/master/2022%20%E6%B5%B7%E6%8A%A5%E9%9B%86.md)后，工作重点就放到了频道建设。年初推了几期[数学专题](https://gitee.com/flame-ai/siger/blob/master/STEM/%E6%9C%80%E9%9A%BE%E7%A7%91%E6%99%AE%E7%9A%84%E6%95%B0%E5%AD%A6.md)，包括 [PI 主题](https://gitee.com/flame-ai/siger/blob/master/STEM/PI.md)，开始接触 LATEX ，各种学术文章的标准文本格式，进一步研究了 MD 编辑器对其的兼容性，甚至萌生了 MDX 的构想，一切皆可描述。再看到 莫干编辑器 TeXmacs 的关键词，好奇心使然，就进入了。

“数学图表” 绘制工具，集成编辑器，中学数学老师的好帮手，再出题画图表，粘贴 TeXmacs 文本，修改参数就搞定啦。

> 这就是最初的直观印象，随不精确，数学频道的阿婆主就是熊猫啦。没有之二，再契合不过当前的 SIGer 推广开源文化的科普任务的啦。我要发出邀请，把莫干编辑器作为 石榴派 推荐应用。在经过一番试用调研，正式发出采访邀请，就有了本期专题的主要内容：“[代码——纯文本已经满足不了“我”的野心](https://www.zhihu.com/question/55655088/answer/763551807)” 

- 莫干编辑器符合具有特质的开源软件工具，
- 应用群体中学教师，和 SIGer 读者群像高度重叠
- SIGer 起点借助文档协作，推广开源，与墨客星球目标路径完全一致
- 少年频道的主角都是深入开源一线，取得成就的开发者，@[沈浪熊猫儿](https://gitee.com/darcyshen) 就是一位。
- 内容产出和数学频道完全一致，SIGer 数学频道的大使非你莫属

同学们，话不多说，依照篇头的标题依次分享如下：

# [墨干编辑器V1.0.4，第一个里程碑](https://mp.weixin.qq.com/s?__biz=MzIwNTU4NDkzMg==&mid=2247483773&idx=1&sn=d499b6bf4a81d0e9b9115077e3a81e3b)

沈浪熊猫儿 墨者实验室 2022-05-28 10:22 发表于浙江

![输入图片说明](https://images.gitee.com/uploads/images/2022/0618/164842_806dd4c4_5631341.png "屏幕截图.png")

- 微信号: 墨者实验室 xmacs2020
- 功能介绍: Xmacs是基于GNU TeXmacs的定制版。 GNU TeXmacs是一个科技编辑平台，其灵感来源于 LaTeX 和 Emacs。

墨干编辑器V1.0.4是V1.0.x系列最后一个版本，是墨干编辑器的第一个里程碑！点击原文链接可以观看本次发布会视频（视频开头我会介绍如何使用微软的必应搜索引擎找到墨干编辑器的官方代码仓库）。

大家好，我是沈浪熊猫儿，毕业于中国科学技术大学计算机系，从2013年开始参与GNU TeXmacs。GNU TeXmacs是 **法国数学家Joris van der Hoeven** 从上世纪九十年代开始研发的一个结构化编辑器，它的主要用户是科研机构的“高智商”群体。

我于半年前发布墨干编辑器V1.0.0，并创建了墨者实验室这样一个非盈利的松散自由的组织，是为了普及GNU TeXmacs，让所有人都用享用Joris软件作品。

GNU TeXmacs的开发者之一 **Massimiliano Gubinelli** ，他是 **德国波恩大学数学系的教授** ，在新冠疫情肆虐期间，将 GNU TeXmacs 底层脚本引擎切换为S7 Scheme。S7 Scheme和GNU TeXmacs一样有着悠久的历史，也是从上个世纪开始研发的项目，是Scheme编程语言的其中一个实现，它的作者是 **斯坦福大学的教授Bill Schottstaedt** 。 **高性能的S7 Scheme实现，给功能丰富但是性能上略有不足的GNU TeXmacs插上了腾飞的翅膀！** 


我利用工作之余，对GNU TeXmacs和实验性质的S7 Scheme分支做了一些整合，在墨者实验室同伴的协助下，发布了 GNU TeXmacs 的发行版墨干编辑器。



### 墨干编辑器的宗旨
总结一下墨干编辑器V1.0.4和GNU TeXmacs 2.1.1的三大区别：
① 墨干编辑器优先服务教育，尤其是中学教育，基于TeXmacs，从软件本身角度上讲，墨干编辑器会尽力降低TeXmacs的使用门槛。
② 墨干编辑器是墨者实验室旗下结构化编辑器，将会和墨者实验室旗下墨客星球深度集成。墨客星球致力于解决网络上TeXmacs文档匮乏的现状，是科技和教育领域的内容平台。
③ 墨干编辑器V1.0.4基于GNU TeXmacs 2.1.1定制，采用高性能的S7 Scheme脚本引擎，并且已经升级到了Qt 5.15.x。



### 重要变更
- 经过多次迭代，S7 Scheme脚本引擎对于初级用户的常用功能来说，已经十分稳定流畅
- 通过 帮助→墨客星球 ，只要连接互联网，就可以免费获得
  - 2021年十份高考数学试题真题
  - 基础数学结构和希腊字母的输入方式，并配有B站教学视频合集（共六个10分钟以内的视频）
- 插件： 修复Gnuplot插件若干问题，并录制Gnuplot插件安装和使用两个教学视频，可用于中学教育中函数图像的绘制
- 插件：  内置200K以内的欧几里得绘图软件可执行文件，欧几里得插件默认可用（Windows平台仍需从应用商店安装Python），可用于中学教育中的平面几何教学
- 修复了困扰TeXmacs中文用户多年的中文输入法漏字（非期望提交上屏）的问题
- 修复了 帮助→查找→文档 弹窗使用中文输入法直接崩溃的问题
- 修复了远程文档中非TeXmacs文档链接的跳转问题，V1.0.4开始，非TeXmacs文档链接可以直接跳转到浏览器打开
- 修复了分段函数的排版问题，由于该修复是临时解决方案，暂时没有反馈到上游GNU TeXmacs代码仓库
- 易用性： 在右键菜单中顶部增加了复制、剪切、粘贴三个菜单项，更加符合用户日常使用习惯
- 易用性： 在中文文档中，新增一系列按键序列转化以方便输入①②③，例如：@1用于输入①
- 易用性： 调整了一些TeXmacs的默认配置，比如默认使用Emacs快捷键，具体请看发布会视频

### 致谢

- 感谢中国美院的鹿尔尔女士为墨干编辑器V1.0.0创作了全新的图标的第一个版本
- 感谢中科院某所研究员自2013年以来对GNU TeXmacs中文社区的贡献
- 感谢墨者实验室目前所有成员
- 感谢Gitee提供的代码托管平台，并帮助我们自动发布软件新闻到开源中国
- 感谢CSDN旗下GitCode代码托管平台提供的高速软件安装包下载服务
- 感谢Github免费的Github Action

<div class="committed-info" style="margin-top: 20px;">
<span class="text-muted">最后提交信息为：</span>
<a href="/XmacsLabs/mogan/commit/65cecce8320043de54f989473cc130f1d21a8788">debian packager for v1.0.4</a>
</div>
</div>
<div class="footer">
<div class="title">下载</div>
<div class="ui celled list releases-download-list">
<div class="item">
<a href="/XmacsLabs/mogan/attach_files/1077308/download/Mogan-v1.0.4-64bit-installer.exe"><i class="file archive outline icon"></i>
Mogan-v1.0.4-64bit-installer.exe
</a></div>
<div class="item">
<a href="/XmacsLabs/mogan/attach_files/1077309/download/Mogan-v1.0.4-32bit-installer.exe"><i class="file archive outline icon"></i>
Mogan-v1.0.4-32bit-installer.exe
</a></div>
<div class="item">
<a href="/XmacsLabs/mogan/attach_files/1077310/download/Mogan_v1.0.4.dmg"><i class="file archive outline icon"></i>
Mogan_v1.0.4.dmg
</a></div>
<div class="item">
<a class="download-archive" data-ref="v1.0.4" data-sha="65cecce8320043de54f989473cc130f1d21a8788" data-format="zip" href="/XmacsLabs/mogan/repository/archive/v1.0.4?format=zip"><i class="file archive outline icon"></i>
下载
Source code (zip)
</a></div>
<div class="item">
<a class="download-archive" data-ref="v1.0.4" data-sha="65cecce8320043de54f989473cc130f1d21a8788" data-format="tar.gz" href="/XmacsLabs/mogan/repository/archive/v1.0.4?format=tar.gz"><i class="file archive outline icon"></i>
下载
Source code (tar.gz)
</a></div>
</div>
</div>
</div>

当前(2022/05/28)，最新的墨干编辑器是V1.0.4

前往下载墨干编辑器最新版本：
https://gitee.com/XmacsLabs/mogan/releases/

<img width="169px" src="https://images.gitee.com/uploads/images/2022/0618/164713_de34d775_5631341.png">

<div class="release-body">
<div class="release-header">
<a target="_blank" class="title" href="/XmacsLabs/mogan/releases/v1.0.4">墨干编辑器V1.0.4发布了</a>
</div>
<div class="meta text-muted" style="margin-bottom: 10px;">
<span class="author js-popover-card" data-username="darcyshen">
<a href="/darcyshen"><img class="ui avatar image mini" width="16" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAASdJREFUOE/tk6GLwgAUhz/tsjUtbrC/wGBxLFmWXLOMFQc2gwPBYjCsKGJQ/wCtCguCYcGVxcUN7BazLJlOtnAg3onHhSv36uN9/N73eAXgg19U4R/Ak4Nut8v5fMb3/Vzter1mt9txOBy+VP0EsG2bXq/H8XikXC6jqipxHJOmKUmSMJ1OH0APgFKpRKfTwfM85vN5PmAYBvv9nkqlQrVapdVqfQ/IOqvVinq9zna7JYoixuNxvsLlckGWZRaLxWtAlqJWqxGGIaZpMhqNuF6vNBqN9xy0220Gg0Huod/vIwgCoiiiadp7gNlshqIonE4nms0mm80Gy7JwHIfJZILrugRB8Al7ukImrFgsIkkSw+GQ2+3GcrnMU2RSdV1/7eCnf/X3z3QHoCFwAXoJMGsAAAAASUVORK5CYII=" alt="沈浪熊猫儿-darcyshen"></a>
<span class="name">沈浪熊猫儿</span>
</span>
</div>
<div class="content">
<div class="markdown-body">
<h2>马上下载</h2>
<table>
<thead>
<tr>
<th>点我下载</th>
<th>系统</th>
<th>MD5校验</th>
</tr>
</thead>
<tbody>
<tr>
<td><a href="https://gitee.com/link?target=https%3A%2F%2Fgitcode.net%2FXmacsLabs%2Fmogan%2Fuploads%2F089b3b73cd4fbbaa88ff170c0c3f1866%2FMogan-v1.0.4-64bit-installer.exe" rel="nofollow noreferrer noopener" target="_blank">点我</a></td>
<td>Windows 64位</td>
<td>02521cef7dfacc65d0a2e6306d1abb97</td>
</tr>
<tr>
<td><a href="https://gitee.com/link?target=https%3A%2F%2Fgitcode.net%2FXmacsLabs%2Fmogan%2Fuploads%2F3d49cab60a89988815c658d7e2dc9326%2FMogan-v1.0.4-32bit-installer.exe" rel="nofollow noreferrer noopener" target="_blank">点我</a></td>
<td>Windows 32位</td>
<td>8d3f4dade7c6a20b9c905eba33bc9962</td>
</tr>
<tr>
<td><a href="https://gitee.com/link?target=https%3A%2F%2Fgitcode.net%2FXmacsLabs%2Fmogan%2Fuploads%2F2d713e9f9f19b9120f34e3b2176fcc36%2FMogan_v1.0.4.dmg" rel="nofollow noreferrer noopener" target="_blank">点我</a></td>
<td>macOS &gt;= 10.15</td>
<td>6a4ec1caed42e052461384deeafd441f</td>
</tr>
</tbody>
</table>
<h2>马上观看<a href="https://gitee.com/link?target=https%3A%2F%2Fwww.bilibili.com%2Fvideo%2FBV1jY4y1L7eX" rel="nofollow noreferrer noopener" target="_blank">发布会视频</a>
</h2>


![输入图片说明](https://images.gitee.com/uploads/images/2022/0618/171634_8bb9267e_5631341.png "屏幕截图.png")

https://www.bilibili.com/video/BV1jY4y1L7eX

一个很棒的发布会！

# 代码——纯文本已经满足不了“我”的野心

### 1. 做两个底层替换的直接原因？

因为Qt 4.8.x已经不在维护状态了，Qt最新的版本是Qt 6.x，所以我们选择升级到Qt 5.15.x。然而上游还没有做迁移到Qt 5的具体计划。目前Qt 4.8.x仍旧在各大Linux发行版的软件源里面，我认为我们不能等到Qt 4.8.x在Linux发行版里面被淘汰了，再升级到Qt 5。我需要在墨干编辑器里面做一些试验，来证明迁移到Qt 5不会带来新的问题，然后说服上游迁移到Qt 5。

Guile 1.8.x目前已经不在Debian的软件源里面（这是GNU TeXmacs这么多年没有在Debian软件源里面的最重要的原因），但是Guile 2.x性能上有问题，Guile 3.x在性能上能满足要求，但是暂时还不能在Windows平台正常工作。而S7 Scheme，这个小巧且高性能的实现，能够满足我们的需求。目前，墨干编辑器迁移到S7 Scheme，对于最终用户的影响不大，但是对于资深用户是有一定影响的。和Qt 5一样，我们需要在墨干编辑器中证明，S7 Scheme的切换足够稳定、高性能，最终才能去说服上游切换到S7 Scheme或者新增一个Scheme实现。

最近，我们在尝试将墨干编辑器投递进入Debian软件源。

### 2. 做 TEXMACS 上游社区工作的背景故事？

我在中国科学技术大学计算机系求学期间，是USTC LUG的活跃成员。我从初中开始学习编程，高中有了自己的电脑就开始折腾Ubuntu。我始终认为软件自由是很重要的！在大学期间，我一直想参与开源社区。然后，在2013年，我了解大学同学那边了解到GNU TeXmacs，开始用GNU TeXmacs完成自己的作业报告（用PDF格式提交）。GNU TeXmacs里面的代码片段、二叉树等功能很好地满足了我的需求。我当时看到GNU TeXmacs的官方文档没有中文翻译，就抽空翻译官方文档，借着文档翻译，我对这个软件了解了更多，我觉得这是一个对大学生的日常学习非常有帮助的软件。所以，我联系了GNU TeXmacs官方，得到了代码提交的权限，把我翻译的部分文档提交到了GNU TeXmacs的代码仓库。

大学期间，我大部分时间还是在翻译文档，虽然我有整个代码仓库的代码提交权限，我把自己的贡献范围缩小在文档翻译这个目录下面。本科毕业，参加工作之后，有了很多做软件工程经验，我才开始有计划地给GNU TeXmacs贡献代码。在早期，我主要在GNU TeXmacs的插件做一些力所能及的工作，另外会修复一些GNU TeXmacs的错误，尤其是和CJK相关的一些错误。

后来，随着编程经验上的不断的丰富，在工作中，已经有能力参与Apache Spark这样的大型项目的开发，自己也基于Apache Spark的Catalyst引擎研发过SQL引擎。在工作实践中，我对如何维护一个项目有了自己看法，我试着把我自己在工作中行之有效的实践应用到GNU TeXmacs研发当中，比如说在GNU TeXmacs代码仓库中引入C++的单元测试。GNU TeXmacs这个项目事实上是比现在这些常见的软件工程开发方法（比如测试驱动开发）的诞生更早。GNU TeXmacs的代码实现相对来说是比较高质量的，在这样一个和Apache Spark项目差不多规模的项目中引入“现代的开发方法”并不是一件公认的正确的决策。不过测试代码并不影响功能代码，所以我仍旧把这种开发方法或者习惯带入到了GNU TeXmacs的日常研发中。然后，在一些编程理念上，我和GNU TeXmacs的作者是有不同的看法的，比如将Qt升级到Qt 5。

为了不被GNU TeXmacs这个代码仓库的规则所束缚，所以我自己基于GNU TeXmacs制作了自己的发行版，一开始叫Xmacs，因为我是一个软件工程师，并不是LaTeX用户，我取名Xmacs的最初的想法就是我只关注TeXmacs中TeX之外的那些功能。今年（2022年）开始我把Xmacs更名为墨干编辑器，一方面近两年长期在家办公，自然而然更加关注家乡的山水，用墨干（音同莫干山的莫干）来表达我自己的“乡愁”，另一方面，我觉得GNU TeXmacs这个名字可能是影响它被更多的用户所接纳的一个重要原因，针对中文用户改名墨干，我可以在Bilibili的原创视频中，自信满满地念出这个名字，也让中文用户更能接受这个名字。我们在墨干探索GNU TeXmacs发展的方向，同时在及时地将在墨干中比较可靠的实现提交到上游代码仓库中。

### 3. 《Scala实用指南》的翻译工作是怎样的背景？

在日常工作中经常使用Scala，自然而然会去参与Scala语言社区，结识一些志同道合的朋友。当时在Scala中文社区，了解到淘宝虎鸣要翻译《Pragmatic Scala》，我就联系他和他一起翻译并出版了《Scala实用指南》。翻译是很辛苦的事情。我翻译这本书，其实也带着通过实践去了解出版一本书的整个流程的想法，因为我一直有写一本GNU TeXmacs相关图书的计划。

### 4. 两篇知乎文章落款都是 TEXMACS，当前的工作重心是怎样的？
> 墨干是怎么水到渠成的？还是一时的灵感？

（前面的回到已经充分说明了缘起，以及阿婆主的心愿，一直心中的期待： _因为我一直有写一本GNU TeXmacs相关图书的计划！_ ）

### 5. 结构化编辑器一文的回答，“代码——纯文本已经满足不了“我”的野心”
> 代表了一颗改变世界的心，是一个程序员应有的朝气。
> 用它来做本次采访的标题是否可以？

可以

### 如果做一个自我评价或者给同学们介绍自己，会用什么语言来形容自己？

我的母语：吴语。

### 6. MILL SCALA 这是在 RISCV 社区认识的，
> 阅读了一些背景后， SCALA 的作用面远不于此？
> 对于青少年，是否也会有这样的怪圈“喜欢 Scala 的程序员爱不释手；玩不来的则谈之色变。”
> **对于学习之路，青少年面前越来越长，有什么好的建议吗？** 

纸上得来终觉浅，绝知此事要躬行。我觉得编程语言脱离实践是没有意义的，一门优秀的语言，必然是活着的，换句话说就是，有这样一个群体，在用这门编程语言进行创作。

### 7. 墨客星球更多是社区外围建设的工作，就开发方面，还有那些工作？包括野心？是否可以引入一些青少年？当然门槛确实高。

社区是我们墨者实验室工作的重心。墨客星球是在为墨者百科做一些社区建设和技术上的探索。墨干编辑器（GNU TeXmacs）只是一个编辑器（工具），我们用这个工具创作的内容才是椟中之珠。

目前在开发上还有大量工作，其中最重要的就是，目前向墨客星球提交内容需要遵循Git工作流，Git工作流本身是软件开发的标准工作流，是常识。但是非开发者来说，Git工作流过于繁琐，我们想在墨干编辑器中直接编辑完成之后，点击提交按钮，就完成了Git代码合并请求的提交。这样，没有工程背景的墨干编辑器用户，也能够很方便地在墨客星球创建自己的空间。

我们希望具有探索精神的青少年，能够加入我们。一开始可以是B站视频的观众，墨客星球的阅读者，慢慢地，我希望他们可以成为墨客星球某个空间的主人！

### 8. 最后，保留问题？《我的兴趣爱好》是 SIGER 编委的必答题，也是同学们之间相互认识一个基础。也邀请您介绍一下自己的兴趣爱好？

我喜欢游泳，也喜欢和朋友们一起自驾游。

### 9. 除了对学生，SIGER 读者群也会有老师？除了墨客星球的社区建设，多用，多分享，就“创新”的理解是否有更具体的思路？或者和墨客星球社区是如何结合的？“学习既有科学技术，创造全新科学技术。”

我们希望老师们能够愉快地使用墨干编辑器，也特别希望能够收到老师们一些针对细节的反馈，正面的或者负面的。目前，墨干编辑器的目标用户中，我们会优先服务中学教师这个群体。

墨客星球目前和中学教师们最大的联系是内置的可编辑可另存为的高考数学真题（2021年和2022年），这是目前第一个结合点。我们希望能得到一线教师的反馈，比如针对试卷内容的纠错，也希望互联网最初的那种无私的分享精神能够在墨客星球中复兴。

第二个结合点是，我们后续会在墨客星球中内置一些幻灯片模版，这样老师们可以访问墨客星球，就可以获得这些模版，用于日常目的。比如，Yiqi Xu上传的视频中这个黑板和白色粉笔风格的幻灯片模版：https://www.zhihu.com/zvideo/1508534867768266752

第三个结合点是，我们后续会花一些功夫在墨干编辑器V1.0.4内置的欧几里得绘图插件上，写一些教程，制作一些视频，告诉老师们将欧几里得绘图软件用于平面几何的教学。教程会和视频配套，访问墨客星球，老师们就可以很方便地将视频中的绘图代码复制出来，稍作修改，用于教学。

