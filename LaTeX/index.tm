<TeXmacs|2.1.1>

<style|<tuple|generic|chinese>>

<\body>
  <\hide-preamble>
    <assign|myspace|http://git.tmml.wiki/XmacsLabs/planet/raw/main/LaTeX/>

    <assign|gitlink|<macro|name|<hlink|<arg|name>|<merge|<value|myspace>|<arg|name>|.tm>>>>
  </hide-preamble>

  <doc-data|<doc-title|<LaTeX>\<#7B26\>\<#53F7\>\<#5927\>\<#5168\>>>

  <section*|\<#7B26\>\<#53F7\>\<#5217\>\<#8868\>>

  <\itemize>
    <item><gitlink|BinaryOperators>
  </itemize>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|?|1|../../../.Xmacs/texts/scratch/no_name_2.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|\<#7B26\>\<#53F7\>\<#5217\>\<#8868\>>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>